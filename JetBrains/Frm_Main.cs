﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using JetBrains.Libs;

namespace JetBrains
{
    public partial class Frm_Main : Form
    {
        private List<Soft> Softs { get; set; } = new List<Soft>();
        public Frm_Main()
        {
            InitializeComponent();
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("操作前需要关闭所有JetBrains软件，是否继续？", "提示", MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                this.Enabled = false;
                try
                {
                    Soft current = cbb_softs.SelectedItem as Soft;
                    if (current == null)
                    {
                        foreach (var item in Softs)
                        {
                            item.Do();
                        }
                    }
                    else
                    {
                        current.Do();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, "提示", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    this.Enabled = true;
                }
            }
        }

        private void Frm_Main_Load(object sender, EventArgs e)
        {
            Softs.AddRange(SystemHelper.GetAllSofts());
            foreach (var item in Softs)
            {
                cbb_softs.Items.Add(item);
            }
            cbb_softs.SelectedIndex = 0;
        }
    }
}
