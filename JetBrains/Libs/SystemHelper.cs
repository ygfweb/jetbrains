﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JetBrains.Libs
{
    public static class SystemHelper
    {
        public static DirectoryInfo GetJetBrainsDirectory()
        {
            string appDataPath = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData);
            string path = Path.Combine(appDataPath, "JetBrains");
            DirectoryInfo directory = new DirectoryInfo(path);
            return directory;
        }

        public static List<Soft> GetAllSofts()
        {
            List<Soft> items = new List<Soft>();
            var d = GetJetBrainsDirectory();
            if (d.Exists)
            {
                List<DirectoryInfo> directoryInfos = d.GetDirectories().ToList();
                foreach (var item in directoryInfos)
                {
                    if (Regex.IsMatch(item.Name, @"^[a-zA-Z]+(\d+){4}\.\d{1,2}$"))
                    {
                        string name = RegexHelper.GetMatchString(item.Name, @"[a-zA-Z]+");
                        string version = RegexHelper.GetMatchString(item.Name, @"(\d+){4}\.\d{1,2}");
                        Soft soft = new Soft(name, version);
                        items.Add(soft);
                    }
                }
            }
            return items;
        }
    }
}
