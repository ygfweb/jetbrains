﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Win32;

namespace JetBrains.Libs
{
    public class Soft
    {
        public string Name { get; }
        public string Version { get; }

        public Soft(string name, string version)
        {
            if (string.IsNullOrWhiteSpace(name))
            {
                throw new Exception("软件名称不能为空");
            }
            this.Name = name;
            this.Version = version;
        }

        public DirectoryInfo GetDirectoryInfo()
        {
            string rootPath = SystemHelper.GetJetBrainsDirectory().ToString();
            string softPath = Path.Combine(rootPath, this.Name + this.Version);
            return new DirectoryInfo(softPath);
        }

        /// <summary>
        /// 删除Eval目录
        /// </summary>
        public void RemoveEvalDirectory()
        {
            string path = Path.Combine(GetDirectoryInfo().ToString(), "eval");
            DirectoryInfo d = new DirectoryInfo(path);
            if (d.Exists)
            {
                d.Delete(true);
            }
        }

        /// <summary>
        /// 删除other.xml
        /// </summary>
        public void RemoveXmlFile()
        {
            string path = Path.Combine(GetDirectoryInfo().ToString(), "options", "other.xml");
            FileInfo file = new FileInfo(path);
            if (file.Exists)
            {
                file.Delete();
            }
        }

        /// <summary>
        /// 删除注册表
        /// </summary>
        public void RemoveReg()
        {
            // HKEY_CURRENT_USER\Software\JavaSoft\Prefs\jetbrains\phpstorm
            RegistryKey registryKey = Registry.CurrentUser;
            RegistryKey currentKey = registryKey.OpenSubKey(@"Software\JavaSoft\Prefs\jetbrains", true);
            currentKey.DeleteSubKeyTree(this.Name.ToLower(), false);
        }

        /// <summary>
        /// 破解试用
        /// </summary>
        public void Do()
        {
            this.RemoveEvalDirectory();
            this.RemoveXmlFile();
            this.RemoveReg();
        }

        public override string ToString()
        {
            return this.Name + this.Version;
        }
    }
}
