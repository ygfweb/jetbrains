﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace JetBrains.Libs
{
    public static class RegexHelper
    {
        public static string GetMatchString(string text, string pattern)
        {
            Regex reg = new Regex(pattern, RegexOptions.IgnoreCase);
            MatchCollection matches = reg.Matches(text);
            foreach (Match item in matches)
            {
                return item.Value;
            }
            return "";
        }
    }
}
